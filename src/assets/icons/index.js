/* eslint-disable */
require('./bg')
require('./block_2_1')
require('./block_2_2')
require('./block_2_3')
require('./block_2_info')
require('./block_3_1')
require('./block_3_2')
require('./block_3_3')
require('./block_3_4')
require('./block_3_5')
require('./block_3_arrow')
require('./button_arrow')
require('./car')
require('./cloud_large')
require('./cloud_small')
require('./icon_1_c')
require('./icon_1_w')
require('./icon_2_c')
require('./icon_2_w')
require('./icon_3_c')
require('./icon_3_w')
require('./icon_4_c')
require('./icon_4_w')
require('./icon_5_c')
require('./icon_6')
require('./label_schedule')
require('./link')
require('./logo_dark')
require('./logotype')
require('./operator_1')
require('./operator_2')
require('./operator_3')
require('./testimonials_arrow_left')
require('./testimonials_arrow_right')
require('./weralls_logo')
